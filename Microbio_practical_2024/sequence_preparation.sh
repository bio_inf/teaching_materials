# Processing amplicon sequencing data

# Log in to the bio-48 server
ssh gast@bio-48

# You will be placed in your home directory.
# There you will find a directory named after the sequencing output

# Explore the contents of the directory using the command: cd and ls
# More info on navigating the linux command line: https://git.io-warnemuende.de/bio_inf/tutorials_collection/src/branch/master/Linux_intro_2022/linux_intro_slides.pdf

# Each group should create a separate directory in the home directoy to store any output files in
# First, if you are not already there, go back to home
cd ~ 
# Then create the directory, replace N with group number
mkdir GroupN 
# Go to the group directory, all subsequent analysis steps will be conducted from there
cd GroupN


### Step 1: Inspect sequence quality

# There are multiple tools to visualize sequence quality.
# Here, we will use the program fastqc (https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
# This program is java-based and should run on most operating systems

# We need to load the software environment that holds fastqc
conda activate qc-env

# To find out more about how to use fastqc:
fastqc -h

# In the command we need to specify the input files, the output directory, and the number of threads (parallel processes)
# Create directory to collect fastqc output
mkdir Fastqc_out
# Run fastqc
# The path to the input data needs to be adjusted to the sequencing output from the practical
# The asterisk is a so-called wildcard that will be expanded to all available files in the directory with the ending .fastq.gz
fastqc -o Fastqc_out -t 6 ../90-1048683967/raw_fastq/*.fastq.gz

# To view the output, the html files can be transfered to the local drive and opened in the browser
# To transfer files from a remote host (the bio-48 server) to the local drive, open a new session on your local drive and run: scp
scp gast@bio-48:~/GroupN/Fastqc_out/*.html ./

# Inspect the quality of the sequences
# Which elements of the fastqc output are most informative for amplicon data?


### Step 2: Remove artificial sequences

# In case the PCR did not work well, there is a chance that there are still sequencing adapters included in the data
# To remove them, we will work with bbduk from the bbmap suite (https://sourceforge.net/projects/bbmap/)
conda activate bbmap-39.01

# You can also few additional information by just running the command without any input
bbduk.sh

# Remove sequencing adapters
# This is a step that needs to be run independently for each sample
# As this is a repetetive action, we can use a loop
# SAMPLE will take the value of each sample name for subsequent iterations of the loop
mkdir Adapter_clipped
for SAMPLE in $(seq 1 12)
do
  bbduk.sh in=../90-1048683967/raw_fastq/${SAMPLE}_R1_001.fastq.gz in2=../90-1048683967/raw_fastq/${SAMPLE}_R2_001.fastq.gz out=Adapter_clipped/${SAMPLE}_adapter_clipped_R1.fastq.gz out2=Adapter_clipped/${SAMPLE}_adapter_clipped_R2.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 threads=8 tpe tbo
done

# Repeat the fastqc quality check after adapter removal.

# Remove primers
conda activate cutadapt-4.1

# You will find the primer sequences in your practical script (adjust as needed)

# Prokaryotes:
# fwd: CCTAYGGGRBGCASCAG
# rev: GGACTACNNGGGTATCTAAT

# Eukaryotes:
# fwd: CCAGCASCYGCGGTAATTCC
# rev: ACTTTCGTTCTTGATYRA

# The primers are supplied via -g (located at 5' end of R1) and -G (located at 5' end of R2)
# The ^ symbol means that the primers are located at the beginning of the respective read
# Since the sequencing adapters were added to the PCR product irrespective of its orientation, both forward-reverse and reverse-forward primer orientations have to be trimmed
mkdir Primer_clipped

# prokaryotes
for SAMPLE in $(seq 1 6)
do
  cutadapt -j 6 --no-indels -e 0.16 -g ^CCTAYGGGRBGCASCAG -G ^GGACTACNNGGGTATCTAAT -m 100 --discard-untrimmed -o Primer_clipped/${SAMPLE}_primer_clipped_fr_R1.fastq.gz -p Primer_clipped/${SAMPLE}_primer_clipped_fr_R2.fastq.gz Adapter_clipped/${SAMPLE}_adapter_clipped_R1.fastq.gz Adapter_clipped/${SAMPLE}_adapter_clipped_R2.fastq.gz
  cutadapt -j 6 --no-indels -e 0.16 -g ^GGACTACNNGGGTATCTAAT -G ^CCTAYGGGRBGCASCAG -m 100 --discard-untrimmed -o Primer_clipped/${SAMPLE}_primer_clipped_rf_R1.fastq.gz -p Primer_clipped/${SAMPLE}_primer_clipped_rf_R2.fastq.gz Adapter_clipped/${SAMPLE}_adapter_clipped_R1.fastq.gz Adapter_clipped/${SAMPLE}_adapter_clipped_R2.fastq.gz
done

# eukaroytes
for SAMPLE in $(seq 7 12)
do
  cutadapt -j 6 --no-indels -e 0.16 -g ^CCAGCASCYGCGGTAATTCC -G ^ACTTTCGTTCTTGATYRA -m 100 --discard-untrimmed -o Primer_clipped/${SAMPLE}_primer_clipped_fr_R1.fastq.gz -p Primer_clipped/${SAMPLE}_primer_clipped_fr_R2.fastq.gz Adapter_clipped/${SAMPLE}_adapter_clipped_R1.fastq.gz Adapter_clipped/${SAMPLE}_adapter_clipped_R2.fastq.gz
  cutadapt -j 6 --no-indels -e 0.16 -g ^ACTTTCGTTCTTGATYRA -G ^CCAGCASCYGCGGTAATTCC -m 100 --discard-untrimmed -o Primer_clipped/${SAMPLE}_primer_clipped_rf_R1.fastq.gz -p Primer_clipped/${SAMPLE}_primer_clipped_rf_R2.fastq.gz Adapter_clipped/${SAMPLE}_adapter_clipped_R1.fastq.gz Adapter_clipped/${SAMPLE}_adapter_clipped_R2.fastq.gz
done

# What to the other parameters in the command mean (-j, --no-indels, -e, -m, --discard-untrimmed) and why are they important?
# How many sequences are kept after each analysis step?
# Determine suitable thresholds for quality trimming (focus on the best position at which to cut the read to remove most of the low quality end)


# Further sequence processing will be conducted in R
# For this, you will need to transfer the primer-clipped reads to your local drive



